package edu.upenn.storage;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.bind.tuple.StringBinding;
import com.sleepycat.je.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;



public class DB {
    private Environment m_env;
    private HashMap<String, Database> m_reduceBoltDb = new HashMap<String, Database>();
    private EnvironmentConfig m_envConfig = new EnvironmentConfig();
    private DatabaseConfig m_dbConfig = new DatabaseConfig();
    private StoredClassCatalog m_classCatalog;
    private Database m_classCatalogDb;

    public DB() {
    }

    public void setup(File envHome, boolean readOnly)
            throws DatabaseException {
        m_envConfig.setReadOnly(readOnly);
        m_dbConfig.setReadOnly(readOnly);

        m_envConfig.setAllowCreate(!readOnly);
        m_dbConfig.setAllowCreate(!readOnly);

        m_envConfig.setTransactional(!readOnly);
        m_dbConfig.setTransactional(!readOnly);

        m_env = new Environment(envHome, m_envConfig);

        m_classCatalogDb = m_env.openDatabase(null, "ClassCatalogDB", m_dbConfig);
        m_classCatalog = new StoredClassCatalog(m_classCatalogDb);
    }

    public void addReduceBoltDb(String id) {
        Database newDb = m_env.openDatabase(null, id, m_dbConfig);
        m_reduceBoltDb.put(id, newDb);
    }

    /**
     * add key value pair to the database specified by id
     * @param id
     * @param key
     * @param value
     */
    public void addKeyValuePair(String id, String key, String value) {
        Database crtDb = m_reduceBoltDb.get(id);
        DatabaseEntry keyEntry = new DatabaseEntry();
        DatabaseEntry dataEntry = new DatabaseEntry();
        StringBinding.stringToEntry(key, keyEntry);
        EntryBinding dataBinding = new SerialBinding(m_classCatalog, SerializableList.class);

        Cursor cursor = crtDb.openCursor(null, null);
        OperationStatus status = cursor.getSearchKey(keyEntry, dataEntry, LockMode.DEFAULT);
        cursor.close();

        if (status == OperationStatus.SUCCESS) {
            SerializableList values = (SerializableList)dataBinding.entryToObject(dataEntry);
            values.arr.add(value);

            dataBinding.objectToEntry(values, dataEntry);
            Transaction txn = m_env.beginTransaction(null, null);
            status = crtDb.put(txn, keyEntry, dataEntry);
            if (status != OperationStatus.SUCCESS) {
                txn.abort();
            }
            txn.commit();
        } else {
            SerializableList values = new SerializableList();
            values.arr.add(value);

            dataBinding.objectToEntry(values, dataEntry);
            Transaction txn = m_env.beginTransaction(null, null);
            status = crtDb.put(txn, keyEntry, dataEntry);
            if (status != OperationStatus.SUCCESS) {
                txn.abort();
            }
            txn.commit();
        }
    }


    /**
     * get the group of value for a particular key
     * @param id
     * @return mapping from key to value array
     */
    public HashMap<String, ArrayList<String>> getAllGroups(String id) {
        Database crtDb = m_reduceBoltDb.get(id);
        HashMap<String, ArrayList<String>> groups = new HashMap<String, ArrayList<String>>();
        Cursor cursor = crtDb.openCursor(null, null);
        DatabaseEntry foundKey = new DatabaseEntry();
        DatabaseEntry foundVal = new DatabaseEntry();
        EntryBinding dataBinding = new SerialBinding(m_classCatalog, SerializableList.class);
        while (cursor.getNext(foundKey, foundVal, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
            String key = StringBinding.entryToString(foundKey);
            SerializableList values = (SerializableList)dataBinding.entryToObject(foundVal);
            groups.put(key, values.arr);
        }
        cursor.close();
        return groups;
    }

    /**
     * clear the entire db
     * @param id
     */
    public void clearDB(String id) {
        Database crtDb = m_reduceBoltDb.get(id);
        Transaction txn = m_env.beginTransaction(null, null);
        DatabaseEntry key = new DatabaseEntry();
        DatabaseEntry value = new DatabaseEntry();
        try (Cursor cursor = crtDb.openCursor(txn, null)) {
            OperationStatus result = cursor.getFirst(key, value, null);
            while ((result == OperationStatus.SUCCESS)) {
                cursor.delete();
                result = cursor.getNext(key, value, null);
            }
        }
        txn.commit();
    }
}

