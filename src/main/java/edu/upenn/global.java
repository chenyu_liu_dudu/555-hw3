package edu.upenn;

public class global {
    public static final String job = "job";
    public static final String inputDir = "inputDir";
    public static final String outputDir = "outputDir";
    public static final String mapExecutors = "mapExecutors";
    public static final String reduceExecutors = "reduceExecutors";
    public static final String spoutExecutors = "spoutExecutors";
    public static final String workerList = "workerList";
    public static final String workerIndex = "workerIndex";
    public static final String mapClass = "mapClass";
    public static final String reduceClass = "reduceClass";
    public static final String jobClass = "jobClass";

}
