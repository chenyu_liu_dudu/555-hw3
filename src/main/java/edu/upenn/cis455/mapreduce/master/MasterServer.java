package edu.upenn.cis455.mapreduce.master;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.MyPrintBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.WordFileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis455.mapreduce.worker.WorkerStatus;
import edu.upenn.global;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static spark.Spark.*;

public class MasterServer {
    private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";
    static Logger logger = LogManager.getLogger(MasterServer.class);
    public static Map<String, WorkerStatus> workerStatusMap = new HashMap<>();
    // Complete list of workers, comma-delimited

    /**
     * Command line parameters:
     * <p>
     * Argument 0: worker index in the workerList (0, 1, ...)
     * Argument 1: non-empty parameter specifying that this is the requestor (so we know to send the JSON)
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        port(45555);
        Config config = new Config();

        if (args.length > 1) {
            config.put("workerIndex", args[0]);

        } else
            config.put("workerIndex", "0");

        // If we're the Master, we need to initiate the computation
        if (args.length == 1) {
            MasterServer.registerStatusPage(config);
            registerWorkerStatusPage(config);
            registerSubmitJobPage(config);
            registerShutDownPage();
            // Let the server start up
        }

    }

    /**
     * register for shudown page, the function will issue a GET request to all living worker servers.
     */
    public static void registerShutDownPage() {
        get("/shutdown", (request, response) -> {
            for (WorkerStatus ws : workerStatusMap.values()) {
                URL url = new URL("http://"+ ws.ip + ":" + ws.port + "/shutdown");
                logger.info("url = " + url.toString());
                try{
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    if (conn.getResponseCode() !=  HttpURLConnection.HTTP_OK) {
                        logger.info("send shutdown to " + url + " failed");
                        conn.disconnect();
                    }
                    else{
                        logger.info("send shutdown to " + url + " success");
                    }
                } catch (Exception e) {}

            }
            System.exit(0);
            return "shutting down master";
        });
    }

    /**
     * registerStatusPage, worker status will be shown on this page as well as the form for job submission
     */
    public static void registerStatusPage(Config config) {
        get("/status", (request, response) -> {
            response.type("text/html");
            String name = "Chenyu Liu/liucy";
            String list = "";
            int counter = 0;
            ArrayList<String> activeWorkers = new ArrayList<>();
            long current = new Date().getTime();
            for (WorkerStatus ws : workerStatusMap.values()) {
                if (current-ws.lastUpdate > 30 * 1000) {
                    logger.info(ws.ip + ":" + ws.port + " is dead");
                }
                else {
                    activeWorkers.add(ws.ip + ":" + ws.port);
                    list +=  "<li>" + (counter++) + ": " + "port=" + ws.port +
                            ", status=" + ws.status +
                            ", job=" + ws.job +
                            ", keysRead=" + ws.keysRead +
                            ", keysWritten=" + ws.keysWritten +
                            ", results=" + ws.results + "</li>";
                }
            }
            config.put("workerList", "[" + String.join(",", activeWorkers) + "]");
            String form = "<form method=\"POST\" action=\"/submitjob\"> " +
                    "Job Name: <input type=\"text\" name=\"jobname\"/><br/> " +
                    "Class Name: <input type=\"text\" name=\"classname\"/><br/> " +
                    "Input Directory: <input type=\"text\" name=\"input\"/><br/> " +
                    "Output Directory: <input type=\"text\" name=\"output\"/><br/> " +
                    "Map Threads: <input type=\"text\" name=\"map\"/><br/> " +
                    "Reduce Threads: <input type=\"text\" name=\"reduce\"/><br/> " +
                    "<input type=\"submit\" value=\"Submit New Job\"/></form>";

            return ("<html>" + name + list + form + "</html>");
        });
    }


    /**
     * post request for submitting a job
     * we will initialize spouts and bolts according to the request params
     * @param config
     */
    public static void registerSubmitJobPage(Config config) {
        post("/submitjob", (request, response) -> {
            System.out.println("map: " + request.queryParams("map"));
            config.put(global.job, request.queryParams("jobname"));
            config.put(global.jobClass, request.queryParams("classname"));
            config.put(global.inputDir, request.queryParams("input"));
            config.put(global.outputDir, request.queryParams("output"));
            config.put(global.mapExecutors, request.queryParams("map"));
            config.put(global.reduceExecutors, request.queryParams("reduce"));
            config.put(global.mapClass, request.queryParams("classname"));
            config.put(global.reduceClass, request.queryParams("classname"));
            config.put(global.spoutExecutors, "1");


            FileSpout spout = new WordFileSpout();
            MapBolt bolt = new MapBolt();
            ReduceBolt bolt2 = new ReduceBolt();
            MyPrintBolt printer = new MyPrintBolt();

            TopologyBuilder builder = new TopologyBuilder();

            // Only one source ("spout") for the words
            builder.setSpout(WORD_SPOUT, spout, Integer.valueOf(config.get("spoutExecutors")));

            // Parallel mappers, each of which gets specific words
            builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(config.get(global.mapExecutors))).fieldsGrouping(WORD_SPOUT, new Fields("value"));

            // Parallel reducers, each of which gets specific words
            builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(config.get(global.reduceExecutors))).fieldsGrouping(MAP_BOLT, new Fields("key"));

            // Only use the first printer bolt for reducing to a single point
            builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);

            Topology topo = builder.createTopology();

            WorkerJob job = new WorkerJob(topo, config);

            ObjectMapper mapper = new ObjectMapper();
            mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
            try {
                String[] workers = WorkerHelper.getWorkers(config);
                logger.info("workers = " + Arrays.toString(workers));
                int i = 0;
                for (String dest : workers) {
                    config.put("workerIndex", String.valueOf(i++));
                    if (sendJob(dest, "POST", config, "definejob",
                            mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() !=
                            HttpURLConnection.HTTP_OK) {
                        throw new RuntimeException("Job definition request failed");
                    }
                }
                for (String dest : workers) {
                    if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() !=
                            HttpURLConnection.HTTP_OK) {
                        throw new RuntimeException("Job execution request failed");
                    }
                }
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.exit(0);
            }

            return "";
        });
    }

    /**
     * register worker page is accessed every 10 seconds by workers to update the status
     * add workers to workerstatusmap
     * @param config
     */
    public static void registerWorkerStatusPage(Config config) {
        get("/workerstatus", (request, response) -> {
            String address = request.ip() + ":" + Integer.parseInt(request.queryParams("port"));
            //update the workerStatus
            WorkerStatus workerStatus = workerStatusMap.get(address);
            if (workerStatus == null) {
                workerStatus = new WorkerStatus();
            }
            workerStatus.ip = request.ip();
            workerStatus.status = request.queryParams("status");
            workerStatus.port = Integer.parseInt(request.queryParams("port"));
            workerStatus.job = request.queryParams("job");
            workerStatus.keysRead = request.queryParams("keysRead");
            workerStatus.keysWritten = request.queryParams("keysWritten");
            workerStatus.results = request.queryParams("results");
            workerStatus.lastUpdate = new Date().getTime();
            workerStatusMap.put(address, workerStatus);
            if (WorkerHelper.getWorkers(config) == null) {
                String newWorkerConfig = "[" + address + "]";
                config.put("workerList", newWorkerConfig);
            } else {
                ArrayList<String> listOfWorkers = new ArrayList<>(Arrays.asList(WorkerHelper.getWorkers(config)));
                if (!config.get("workerList").contains(address)) {
                    listOfWorkers.add(address);
                    String newWorkerConfig = "[" + String.join(",", listOfWorkers) + "]";
                    config.put("workerList", newWorkerConfig);
                }
            }
            logger.info("workerList after register = " +  config.get("workerList"));
            return "register worker OK" ;
        });
    }


    /**
     * issue a http connection to the destination
     * @param dest
     * @param reqType
     * @param config
     * @param job
     * @param parameters
     * @return HTTPURLconnection object
     * @throws IOException
     */
    static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {
        URL url = new URL(dest + "/" + job);
        System.out.println("Sending request to " + url.toString());
        logger.info("Sending request to " + url.toString());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(reqType);
        if (reqType.equals("POST")) {
            conn.setRequestProperty("Content-Type", "application/json");
            OutputStream os = conn.getOutputStream();
            byte[] toSend = parameters.getBytes();
            os.write(toSend);
            os.flush();
        } else
            conn.getOutputStream();
        return conn;
    }
}



