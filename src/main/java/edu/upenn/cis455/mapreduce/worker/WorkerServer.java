package edu.upenn.cis455.mapreduce.worker;

import static spark.Spark.*;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import edu.upenn.storage.DB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.RunJobRoute;
import org.eclipse.jetty.util.IO;
import spark.Spark;

/**
 * Simple listener for worker creation 
 * 
 * @author zives
 *
 */
public class WorkerServer {
    static Logger logger = LogManager.getLogger(WorkerServer.class);

    public static DistributedCluster cluster = new DistributedCluster();

    List<TopologyContext> contexts = new ArrayList<>();

    static List<String> topologies = new ArrayList<>();

    TopologyContext currentContext = null;
    String currentjob = null;
    public static DB db = new DB();
    public static String storeDir = null;
    public static String masterAddress = null;
    public static int myport;
    public TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            String url = "http://" + masterAddress + "/workerstatus";
            try {
                url += "?port=" + myport +
                        "&status=" + getStateString() +
                        "&job=" + URLEncoder.encode(getCurrentJobString(), "UTF-8") +
                        "&keysRead=" + getKeysRead()+
                        "&keysWritten=" + getKeysWritten() +
                        "&results=" + URLEncoder.encode(getResults(), "UTF-8");
                System.out.println("url = " + url);
                URL requestUrl = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) requestUrl.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    logger.error("request failed");
                }
            } catch (Exception e) {
                logger.error("exception at try " + e.getMessage());
            }

        }
    };

    /**
     * constructor of WorkerServer
     * setup Database for worker, set timer for status update,
     * @param myPort
     * @param masterAddr
     * @param storeDir
     * @throws NoSuchAlgorithmException
     */
    public WorkerServer(int myPort, String masterAddr, String storeDir) throws NoSuchAlgorithmException {
        WorkerServer.storeDir = storeDir;
        WorkerServer.masterAddress = masterAddr;
        WorkerServer.myport = myPort;
        logger.info("master address " + WorkerServer.masterAddress);
        File envHome = new File(WorkerServer.storeDir + "/database/");

        envHome.mkdirs();
        db.setup(envHome, false);
        logger.info("Creating server listener at socket " + myPort);
        port(myPort);
        Timer timer = new Timer("statusTimer");
        timer.scheduleAtFixedRate(timerTask, 30, 10000);

        final ObjectMapper om = new ObjectMapper();
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        Spark.post("/definejob", (req, res) -> {
            WorkerJob workerJob;
            try {
                workerJob = om.readValue(req.body(), WorkerJob.class);
                try {
                    logger.info("Processing job definition request" + workerJob.getConfig().get("job") +
                            " on machine " + workerJob.getConfig().get("workerIndex"));
                    currentjob = workerJob.getConfig().get("job");
                    // before adding topologies, clear previous topology
                    synchronized (topologies) {
                        topologies.clear();
                        contexts.clear();
                        topologies.add(currentjob);
                    }
                    // Add a new topology
                    currentContext = cluster.submitTopology(currentjob, workerJob.getConfig(), workerJob.getTopology());
                    contexts.add(currentContext);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return "Job launched";
            } catch (IOException e) {
                e.printStackTrace();
                // Internal server error
                res.status(500);
                return e.getMessage();
            } 
        });


        Spark.post("/runjob", new RunJobRoute(cluster));

        Spark.post("/pushdata/:stream", (req, res) -> {
            try {
                String stream = req.params(":stream");
                Tuple tuple = om.readValue(req.body(), Tuple.class);
                // Find the destination stream and route to it
                StreamRouter router = cluster.getStreamRouter(stream);
                if (contexts.isEmpty())
                    logger.error("No topology context -- were we initialized??");
                TopologyContext ourContext = contexts.get(contexts.size() - 1);
                // Instrumentation for tracking progress
                if (!tuple.isEndOfStream())
                    ourContext.incSendOutputs(router.getKey(tuple.getValues()));
                // TODO: handle tuple vs end of stream for our *local nodes only*
                // Please look at StreamRouter and its methods (execute, executeEndOfStream, executeLocally, executeEndOfStreamLocally)
                if (tuple.isEndOfStream()) {
                    router.executeEndOfStreamLocally(currentContext, tuple.getSourceExecutor());
                } else {
                    router.executeLocally(tuple, currentContext, tuple.getSourceExecutor());
                }
                return "OK";
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                res.status(500);
                return e.getMessage();
            }
        });

        Spark.get("/shutdown", (request, response) -> {
            shutdown();
            System.exit(0);
            return "shutting down worker";

        });
    }

    public static void shutdown() {
        synchronized(topologies) {
            for (String topo: topologies)
                cluster.killTopology(topo);
        }
        cluster.shutdown();
    }

    /**
     *
     * @return currentContext.keysRead or null
     */
    private int getKeysRead() {
        if (currentContext == null)
            return 0;
        return currentContext.keysRead.get();
    }

    /**
     *
     * @return currentContext.keysWritten or null
     */
    private int getKeysWritten() {
        if (currentContext == null)
            return 0;
        return currentContext.keysWritten.get();
    }

    /**
     *
     * @return currentContext.results or null
     */
    private String getResults() {
        if (currentContext == null)
            return "[]";
        return currentContext.results.toString();
    }

    /**
     *
     * @return state of current context
     */
    private String getStateString() {
        if (currentContext == null)
            return "IDLE";
        else if (currentContext.getState() == TopologyContext.STATE.IDLE)
            return "IDLE";
        else if (currentContext.getState() == TopologyContext.STATE.MAP)
            return "MAP";
        else if (currentContext.getState() == TopologyContext.STATE.WAITTING)
            return "WAITING";
        else if (currentContext.getState() == TopologyContext.STATE.REDUCE)
            return "REDUCE";
        return null;
    }

    /**
     *
     * @return currentjob or null
     */
    private String getCurrentJobString() {
        if (currentjob == null) return "None";
        return currentjob;
    }

    /**
     * Simple launch for worker server.  Note that you may want to change / replace
     * most of this.
     * 
     * @param args
     * @throws MalformedURLException
     */
    public static void main(String args[]) throws  NoSuchAlgorithmException{
        if (args.length < 3) {
            System.out.println("Usage: WorkerServer [port number] [master host/IP]:[master port] [storage directory]");
            System.exit(1);
        }
        int myPort = Integer.valueOf(args[0]);
        System.out.println("Worker node startup, on port " + myPort);
        System.out.println("args1:" + args[1] + " args2:" + args[2]);
        WorkerServer worker = new WorkerServer(myPort, args[1], args[2]);
    }
}
