package edu.upenn.cis455.mapreduce.worker;


/**
 * WorkerStatus keeps track of all worker status
 */
public class WorkerStatus {
    public String ip;
    public int port;
    public String status;
    public String job;
    public String keysRead;
    public String keysWritten;
    public String results;
    public long lastUpdate;

}
