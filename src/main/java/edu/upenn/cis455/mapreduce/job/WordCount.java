package edu.upenn.cis455.mapreduce.job;

import java.util.Iterator;

import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis455.mapreduce.Context;
import edu.upenn.cis455.mapreduce.Job;
import edu.upenn.cis455.mapreduce.master.MasterServer;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;
import edu.upenn.cis455.mapreduce.worker.WorkerStatus;

public class WordCount implements Job {

	/**
	 * This is a method that lets us call map while recording the StormLite source executor ID.
	 * 
	 */
	public void map(String key, String value, Context context, String sourceExecutor) {
        String[] toks = value.split("\\s+");
        for (String word : toks) {
            context.write(word, "1", sourceExecutor);
        }
    }

	/**
	 * This is a method that lets us call map while recording the StormLite source executor ID.
	 * 
	 */
    public void reduce(String key, Iterator<String> values, Context context, String sourceExecutor) {
        int sum = 0;
        while(values.hasNext()) {
            sum += Integer.parseInt(values.next());
        }
        context.write(key, "" + sum, "");
    }

}
