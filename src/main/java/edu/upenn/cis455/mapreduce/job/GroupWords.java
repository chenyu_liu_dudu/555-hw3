package edu.upenn.cis455.mapreduce.job;

import edu.upenn.cis455.mapreduce.Context;
import edu.upenn.cis455.mapreduce.Job;

import java.util.Iterator;


public class GroupWords implements Job {
    @Override
    public void map(String key, String value, Context context, String sourceExecutor) {
        context.write(value, value, sourceExecutor);
    }

    @Override
    public void reduce(String key, Iterator<String> values, Context context, String sourceExecutor) {
        context.write(key, key, sourceExecutor);
    }

}
