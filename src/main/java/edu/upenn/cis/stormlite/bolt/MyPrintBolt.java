package edu.upenn.cis.stormlite.bolt;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;
import edu.upenn.global;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class MyPrintBolt implements IRichBolt {

    static Logger logger = LogManager.getLogger(MyPrintBolt.class);

    Fields myFields = new Fields();

    String executorId = "[MyPrintBolt-" + UUID.randomUUID().toString().substring(0, 5) + "]";

    ConsensusTracker votesForEos;

    TopologyContext context = null;
    FileWriter fw = null;
    BufferedWriter bw = null;
    boolean sentEos = false;

    @Override
    public void cleanup() {
        // Do nothing
    }

    /**
     * receive data from bolt and write to file
     * @param input
     * @return
     */
    @Override
    public boolean execute(Tuple input) {
        if (!input.isEndOfStream()) {
            if (sentEos) {
                throw new RuntimeException("We received data from " + input.getSourceExecutor() + " after we thought the stream had ended!");
            }
            synchronized (bw) {
                List<Object> output = input.getValues();
                try {
                    String line = "(" + output.get(0) + "," + output.get(1) + ")" + "\n";
                    logger.info("writing ->" + line );
                    bw.write(line);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } else {
            boolean enoughVotes = votesForEos.voteForEos(input.getSourceExecutor());
            if (enoughVotes) {
                context.setState(TopologyContext.STATE.IDLE);
                try {
                    bw.close();
                    fw.close();
                    WorkerServer.cluster.shutdown();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                sentEos = true;
            }
        }
        return true;
    }

    /**
     * create the output directory for writting to file
     * @param stormConf
     * @param context
     * @param collector
     */
    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        // Do nothing
        this.context = context;
        String outputDir = WorkerServer.storeDir + "/" + stormConf.get(global.outputDir) + "/output.txt";
        File envHome = new File(WorkerServer.storeDir + "/" + stormConf.get(global.outputDir) );
        envHome.mkdirs();
        logger.info("OUTPUT DIR: " + outputDir);
        try {
            fw = new FileWriter(outputDir);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        bw = new BufferedWriter(fw);
        int totalReduceBoltPerWorker = Integer.parseInt(stormConf.get("reduceExecutors"));
        int votes = totalReduceBoltPerWorker * (WorkerHelper.getWorkers(stormConf).length - 1)
                + totalReduceBoltPerWorker;
        logger.info("votes for printer = " + votes);
        votesForEos = new ConsensusTracker(votes);
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void setRouter(StreamRouter router) {
        // Do nothing
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(myFields);
    }

    @Override
    public Fields getSchema() {
        return myFields;
    }

}
