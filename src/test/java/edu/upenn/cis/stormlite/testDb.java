package edu.upenn.cis.stormlite;

import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;
import edu.upenn.global;
import edu.upenn.storage.DB;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;


public class testDb {


    @Test
    public  void addtodb() throws NoSuchAlgorithmException {
        File envHome = new File("testDBDir" + "/");
        DB db = new DB();

        envHome.mkdirs();
        db.setup(envHome, false);
        db.addKeyValuePair("id", "key", "val");
    }

    @Test
    public void writeTest() {
        String outputDir = "storage/node1/outDir/output.txt";
//        File envHome = new File(WorkerServer.storeDir + "/" + stormConf.get(global.outputDir) );
//        envHome.mkdirs();
        System.out.println("OUTPUT DIR: " + outputDir);
        FileWriter fw = null;
        BufferedWriter bw;
        try {
            fw = new FileWriter(outputDir);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        bw = new BufferedWriter(fw);
        try{
            bw.write("testing oooo");
            bw.close();
            fw.close();

        } catch (IOException e) {

        }

    }

    @Test
    public void testArrayList() {
        Config config = new Config();
        config.put("workerList", "[http://127.0.0.1:8001,http://127.0.0.1:8002,http://127.0.0.1:8001,127.0.0.1:8002]");
        ArrayList<String> listOfWorkers = new ArrayList<>(Arrays.asList(WorkerHelper.getWorkers(config)));
        String s = "[http://127.0.0.1:8001,http://127.0.0.1:8002,http://127.0.0.1:8001,127.0.0.1:8002]";
        System.out.println(s.contains("127f.0.0.1:8001"));
    }



}
